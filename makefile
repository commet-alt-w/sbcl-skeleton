# project properties 
project_name=sbcl-skeleton
project_binary=$(project_name)
project_asd=$(project_name).asd
project_pwd=$(abspath .)/
dist=bin
lisp=sbcl

# detect os
ifeq ($(OS), Windows_NT)
	windows=$(OS)
endif

# build target
build:
	@echo "building: $(project_name)"
	$(lisp) --eval "(asdf:load-asd #p\"$(project_pwd)$(project_asd)\")" \
			--eval "(ql:quickload :$(project_name))" \
			--eval "(asdf:make :$(project_name))" \
			--quit

# clean target
clean:
    ifdef windows
		@rd /S /Q $(dist)
    else
		@rm -rf $(dist)
    endif

# info
info:
	$(info os: $(OS))
	$(info shell: $(SHELL))