(defpackage sbcl-skeleton
  (:use :cl)
  (:export :main))
(in-package :sbcl-skeleton)

(defun main ()
  (write "hello, world!")
  (terpri))
