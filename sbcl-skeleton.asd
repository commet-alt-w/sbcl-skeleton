(in-package :asdf-user)

#+sb-core-compression
(defmethod asdf:perform ((o asdf:image-op) (c asdf:system))
  (uiop:dump-image (asdf:output-file o c) :executable t :compression t))

(defsystem "sbcl-skeleton"
  :version "0.1.0"
  :author "commet-alt-w <commet-alt-w@protonmail.com>"
  :license "gpl v 3.0"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "main"))))
  :description "hello world sample project"
  :build-operation "program-op"
  :build-pathname "bin/sbcl-skeleton"
  :entry-point "sbcl-skeleton:main"
  :in-order-to ((test-op (test-op "sbcl-skeleton/tests"))))

(defsystem "sbcl-skeleton/tests"
  :author "commet-alt-w <commet-alt-w@protonmail.com>"
  :license "gpl v 3.0"
  :depends-on ("sbcl-skeleton"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "test system for sbcl-skeleton"
  :perform (test-op (op c) (symbol-call :rove :run c)))
