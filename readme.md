# sbcl-skeleton

sample sbcl program with a make file.

*compile to binary*

## dev

build

```shell
$ make
```

clean

```shell
$ make clean
```

info

```shell
$ make info
```

## dev environemnt

* vscode: [alive](https://github.com/nobody-famous/alive)/[alive-lsp](https://github.com/nobody-famous/alive-lsp)
	* notes:
		* needs repl syntax highlighting
		* needs repl autocomplete
		* does not like more than one project using the alive-lsp server
		* repl input loses focus when alt-tabbing out (general vscode issue?)

## run

from compiled binary

```shell
$ ./bin/sbcl-skeleton
or
$ ./bin/sbcl-skeleton.exe
```

using sbcl/asdf

```shell
$ sbcl
* (require :asdf)
* (asdf:load-asd (merge-pathnames "sbcl-skeleton.asd" (uiop:getcwd)))
* (asdf:load-system :sbcl-skeleton)
* (sbcl-skeleton:main)
```

## notes

* do not use `(sb-posix:getcwd)` with `(merge-pathnames ...)`
  - it is broken and stupid and no one says so

## license

[OUI](/license)