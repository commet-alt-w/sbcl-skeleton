(defpackage sampleproject/tests/main
  (:use :cl
        :sampleproject
        :rove))
(in-package :sampleproject/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :sampleproject)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
