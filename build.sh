#!/bin/bash

set -euo pipefail

dir="$(pwd)"

sbcl --eval "(asdf:load-asd \"$dir/sbcl-skeleton.asd\")" \
     --eval "(ql:quickload :sbcl-skeleton)" \
     --eval "(asdf:make :sbcl-skeleton)" \
     --quit

exit 0
